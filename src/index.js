import React from "react";
import { render } from "react-dom";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import "sanitize.css/sanitize.css";

registerServiceWorker();

const target = document.querySelector("#root");

render(
  <div>
    <App />
  </div>,
  target
);
