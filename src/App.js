import React, { Component } from "react";
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import store, { history } from "./store";
import { MuiThemeProvider } from "material-ui/styles";
import MuiTheme from "./MuiTheme";
import "sanitize.css/sanitize.css";
import "./App.css";
import "typeface-roboto";

import AppMain from "./containers/AppMain/AppMain";

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={MuiTheme}>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <AppMain />
          </ConnectedRouter>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
