import { createMuiTheme } from "material-ui/styles";
import red from "material-ui/colors/red";
import blue from "material-ui/colors/blue";

const MuiTheme = createMuiTheme({
  palette: {
    type: "light",
    primary: {
      light: blue[300],
      main: blue[500],
      dark: red[700]
    },
    secondary: {
      light: red[300],
      main: red[500],
      dark: red[700]
    }
  }
});

export default MuiTheme;
