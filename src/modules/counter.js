export const TOGGLE_REQUESTED = "counter/TOGGLE_REQUESTED";

const initialState = {
  count: 0,
  isIncrementing: false,
  isDecrementing: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_REQUESTED:
      return {
        ...state,
        isToggled: true
      };
    default:
      return state;
  }
};

export const toggle = () => {
  return dispatch => {
    dispatch({
      type: TOGGLE_REQUESTED
    });
  };
};
