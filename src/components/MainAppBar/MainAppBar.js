import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import IconButton from "material-ui/IconButton";
import MenuIcon from "material-ui-icons/Menu";
import red from "material-ui/colors/red";
// import { Transform } from "stream";

const styles = {
  root: {
    flexGrow: 1
  },
  flex: {
    flex: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

// window.addEventListener("scroll", function(event) {
//   var bar = document.getElementById('bar');
//   var top = this.scrollY;

//   bar.style.boxShadow = 0;
//   if (top >= 50) {
//     bar.style.boxShadow = "0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12)";
//     console.log(bar.style.boxShadow);
//   } else {
//     bar.style.boxShadow = "0 0 0 0 transparent";
//   }
// }, false);

class MainAppBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transform: -100
    };
  }

  toggleDrawer() {}

  renderFlexibleSpace = () => {
    const flexibleSpaceStyle = {
      height: 100,
      backgroundColor: red["500"],
      transform: "scaleY(" + 1 / this.state.transform + ")"
    };
    console.log(flexibleSpaceStyle);
    return <div id="flexibleSpace" style={flexibleSpaceStyle} />;
  };

  componentDidMount = () => {
    window.addEventListener("scroll", this.handleScroll);
  };

  componentWillUnmount = () => {
    window.removeEventListener("scroll", this.handleScroll);
  };

  handleScroll = event => {
    // Math.max(0.0, 164 / auxScale + maxMiddleScale);
    let scrollTop = window.scrollY,
      itemTranslate = Math.min(0, scrollTop / 3 - 100);
    if (scrollTop < 100) {
      this.setState({
        transform: itemTranslate
      });
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar id="bar" color="default">
          <Toolbar>
            <IconButton
              className={classes.menuButton}
              color="inherit"
              aria-label="Menu"
              onClick={this.toggleDrawer}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="title"
              color="inherit"
              className={classes.flex}
            >
              Главная
            </Typography>
            <Button color="inherit">Login</Button>
          </Toolbar>
          {/* {this.renderFlexibleSpace()} */}
        </AppBar>
      </div>
    );
  }
}

MainAppBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MainAppBar);
