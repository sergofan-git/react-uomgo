import React from "react";
import { Route } from "react-router-dom";
import Home from "../Home/Home";
import About from "../About/About";
import MainAppBar from "../../components/MainAppBar/MainAppBar";
// import MainAppDrawer from '../../components/MainAppDrawer/MainAppDrawer'

const AppMain = () => (
  <div>
    <header>
      <MainAppBar />
    </header>

    <main>
      <Route exact path={process.env.PUBLIC_URL + "/"} component={Home} />
      <Route exact path={process.env.PUBLIC_URL + "/about"} component={About} />
    </main>
  </div>
);

export default AppMain;
