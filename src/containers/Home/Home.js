import React from "react";
import { Link } from "react-router-dom";
import { push } from "react-router-redux";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { toggle } from "../../modules/counter";
import { MainCarousel } from "../Carousel/Carousel";
import LoremIpsum from "../../loremipsum";

const divStyle = {
  textShadow:
    "3px 3px 4px rgba(0,0,0, 0.3), -3px -3px 4px rgba(0,0,0, 0.3), 3px -3px 4px rgba(0,0,0, 0.3), -3px 3px 4px rgba(0,0,0, 0.3), 3px 3px 4px rgba(0,0,0, 0.3)",
  fontWeight: "bolder",
  fontSize: "4vw",
  color: "white",
  textAlign: "center",
  zIndex: 1,
  position: "absolute",
  padding: "16px",
  width: "100%",
  lineHeight: 1
};

const Home = props => (
  <div style={{ margin: "64px 0" }}>
    <div style={divStyle}>
      Управление образованием<br />Мысковского городского округа
    </div>
    <MainCarousel />
    <p>
      <Link to={process.env.PUBLIC_URL + "/"}>Home</Link>
      /
      <Link to={process.env.PUBLIC_URL + "/about"}>About</Link>
    </p>
    <p>{/* <button onClick={props.toggle}>
        Открыть
      </button> */}</p>
    <LoremIpsum />
  </div>
);

const mapStateToProps = state => ({
  isToggled: state.counter.isToggled
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggle,
      changePage: () => push("/about")
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Home);
