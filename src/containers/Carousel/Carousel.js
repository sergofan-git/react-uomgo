import React, { Component } from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

export class MainCarousel extends Component {
  render() {
    return (
      <Carousel
        autoPlay
        showArrows={false}
        showStatus={false}
        showIndicators={false}
        showThumbs={false}
        infiniteLoop={true}
        interval={5000}
        transitionTime={350}
        stopOnHover={false}
        dynamicHeight={true}
        swipeable={true}
        swipeScrollTolerance={7}
      >
        <div>
          <img src="images/Background01.jpg" alt="" />
        </div>
        <div>
          <img src="images/Background02.jpg" alt="" />
        </div>
        <div>
          <img src="images/Background03.jpg" alt="" />
        </div>
        <div>
          <img src="images/Background04.jpg" alt="" />
        </div>
        <div>
          <img src="images/Background05.jpg" alt="" />
        </div>
        <div>
          <img src="images/Background06.jpg" alt="" />
        </div>
        <div>
          <img src="images/Background07.jpg" alt="" />
        </div>
      </Carousel>
    );
  }
}

// ReactDOM.render(<MainCarousel />, document.querySelector('.main-carousel'));

// export default MainCarousel;
